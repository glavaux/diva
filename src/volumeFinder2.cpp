#include <algorithm>
#include <iostream>
#include <cassert>
#include <list>
#include <cmath>
#include <boost/format.hpp>
#include "divaWatershed.hpp"

using boost::format;
using boost::str;

#define NUMDIMS 3

struct TestParticle {
  uint32_t idx;
};

static bool operator<(const TestParticle& p1, const TestParticle& p2)
{
  return p1.idx < p2.idx;
}


//typedef std::list<TestParticle> StateList;
typedef TestParticle *StateList;

using namespace std;

static bool sameIndex(const TestParticle& p1, const TestParticle& p2)
{
  return p1.idx == p2.idx;
}

template<typename T>
static void computeXYZ(T idx, T& x, T& y, T& z, T *dimList)
{
  x = idx % dimList[0];
  y = (idx/dimList[0]) % dimList[1];
  z = idx/(dimList[0]*dimList[1]);
}

static void unique(TestParticle *init, uint32_t& stateLen)
{
  TestParticle *cur = init;
  TestParticle *p = init;
  TestParticle *end = init+stateLen;

  do
    {
      ++p;
      if (!sameIndex(*cur, *p))
        {
          ++cur;
          if (cur != p)
            memcpy(cur, p, sizeof(TestParticle));
        }
    }
  while (p != end);

  stateLen = cur-init;
}

template<typename T, typename F>
static uint32_t doMotion(T *densityField, uint64_t *dimList, 
  uint64_t *connection, std::vector<DivaMinimumInt>& minima, F& progress)
{
//  TestParticle *iter = state;
//  TestParticle *end = state+stateLen;

  uint64_t Nx = dimList[0], Ny = dimList[1], Nz = dimList[2];
  uint32_t numMove = 0;
  uint64_t Nvox = Nx*Ny*Nz;

#pragma omp parallel for schedule(static) reduction(+:numMove)
  for (uint64_t p = 0; p < Nvox; p++) 
    {
      double dZ_0, dZ_1, dY_0, dY_1, dX_0, dX_1;
      uint32_t idx_Z_0, idx_Z_1, idx_Y_0, idx_Y_1, idx_X_0, idx_X_1;
      
      uint64_t x;
      uint64_t y;
      uint64_t z;
      double DeltaMin = 0;
      computeXYZ(p, x, y, z, dimList);

      T d0 = densityField[p];

      int moved = 0;
        
      bool found = false;
      uint32_t minIDX = ~0;
      int minX = x, minY = y, minZ = z;
      for (int iz=-1;iz<=1;iz++)
        {
          int Z = (z+Nz+iz) % Nz;
          for (int iy=-1;iy<=1;iy++)
          {
            int Y = (y+Ny+iy) % Ny;
            for (int ix=-1;ix<=1;ix++)
              {
                int X = (x+Nx+ix) % Nx;
                double q = sqrt(ix*double(ix) + iy*double(iy) + double(iz)*iz);

                uint64_t idx = (Z*Ny + Y)*Nx + X;
                double Delta = (densityField[idx]-d0)/q;
                if (Delta < DeltaMin)
                  {
                    minX = X; minY = Y; minZ = Z;
                    minIDX = idx;
                    DeltaMin = Delta;
                    moved = 1;
                  }
              }
          }
        }      
      if (!moved) {
        DivaMinimumInt m;
        
        m.x = x;
        m.y = y;
        m.z = z;
#pragma omp critical
        minima.push_back(m);
      }

      connection[p] = minIDX;
      numMove += moved;
    }
  

  return numMove;
}

static void checkNeighbours(uint64_t *dimList, int32_t *zoning, uint64_t *connection, 
                     uint64_t idx, uint64_t *zoner, uint64_t &zoner_end)
{
  uint64_t Nx = dimList[0], Ny = dimList[1], Nz = dimList[2];
  uint32_t zone = zoning[idx];
  uint64_t x, y, z;
  computeXYZ(idx, x, y, z, dimList);
  
  for (int iz=-1;iz<=1;iz++)
    {
      int Z = (z+Nz+iz) % Nz;
      for (int iy=-1;iy<=1;iy++)
        {
          int Y = (y+Ny+iy) % Ny;
          for (int ix=-1;ix<=1;ix++)
            {
              int X = (x+Nx+ix) % Nx;
              uint64_t IDX = X + Nx * (Y + Ny * Z);
              
              if (zoning[IDX] >= 0)
                continue;
                
              if (connection[IDX] == idx)
                {
                  zoning[IDX] = zone;
                  zoner[zoner_end++] = IDX;
                }
            }
        }
    }
}

void divaWatershed2(DivaDensity& density, std::vector<DivaMinimumInt>& minima,
                    DivaZones& zones,
                    boost::function1<void,const std::string&> progress)
{
  StateList currentState;
  uint64_t Nvox = density.Nz*density.Ny*density.Nx;
  uint64_t *connection;
  uint64_t *zoner;
  uint64_t zoner_start, zoner_end;
  
  connection = new uint64_t[Nvox];
  zoner = new uint64_t[Nvox];


  minima.clear();
  progress("Moving...");
  uint64_t dimList[3] = { density.Nx, density.Ny, density.Nz };
  doMotion(density.density, dimList, connection, minima, progress); 
  
  for (uint64_t p = 0; p < Nvox; p++)
    {
      zones.zoning[p] = -1;
    }
    
  zoner_start = 0;
  zoner_end = 0;
  progress("Initialization of the zoner...");
  for (int32_t m = 0; m < minima.size(); m++)
    {
      DivaMinimumInt& mi = minima[m];
      uint64_t idx = mi.x + density.Nx * (mi.y + density.Ny * mi.z);
      
      zones.zoning[idx] = m;
      zoner[zoner_end++] = idx;
    }
  
  progress(str(format("Identifying zones (d <= %lg)...") % zones.maxDenGrowth));
  int q= 0;
  do
    {
      if ((q%5)==0)
        progress(str(format(" -- zone_start = %ld, zone_end = %lg") % zoner_start % zoner_end));
      q++;
 
      uint64_t zoner_end_new = zoner_end;
      for (uint64_t p = zoner_start; p < zoner_end; p++)
        {
         if (density.density[zoner[p]] < zones.maxDenGrowth)
            checkNeighbours(dimList, zones.zoning, connection, zoner[p], zoner, zoner_end_new);
        }
      zoner_start = zoner_end;
      zoner_end = zoner_end_new;
     
    }
  while(zoner_end != zoner_start);
  
  delete[] zoner;  
  delete[] connection;
}
