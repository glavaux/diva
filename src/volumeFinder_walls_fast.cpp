#include <cmath>
#include <cassert>
#include <vector>
#include <list>
#include <set>
#include <iostream>
#include <fstream>
#include <boost/function.hpp>
#include <boost/format.hpp>
#include "divaWatershed.hpp"

using boost::format;
using boost::str;

static const bool CHECK_ZONING = true;

using namespace std;

typedef struct {
  int c[3];
} IntCoord;

typedef struct {
  int c[3];
  double d;
  bool saddle;
} DenXel;

struct DenXelComp {
  bool operator()(const DenXel& a, const DenXel& b)
  {
    if (a.d != b.d)
      return a.d < b.d;
    if (a.c[2] != b.c[2])
      return a.c[2] < b.c[2];
    if (a.c[1] != b.c[1])
      return a.c[1] < b.c[1];
    if (a.c[0] != b.c[0])
      return a.c[0] < b.c[0];

    return false;
  }
};

typedef std::set<DenXel,DenXelComp> TreatmentList;
typedef std::vector<IntCoord> CoordVector;

static const int NOZONE =  -1;
static const int SADDLE = -2; 
static const int SADDLE_POINT = 0;
static const int RESORT_ELEMENT = -1;

typedef struct {
  int zoneId;
  bool stopped;
  double densityMinOnSurface;
  double maxDensity;
  TreatmentList contourPixels;
  int subZone1, subZone2;
  int superZone;
  int firstTouchedZone;
} ZoneDescriptor;

typedef std::vector<ZoneDescriptor> ZoneVector;


static bool sameZone(int zoneId, const ZoneVector& zones, const ZoneDescriptor& zd)
{
  // Either we are the zone
  if (zoneId == zd.zoneId)
    return true;

  // Or it is one the subzone
  if (zd.subZone1 >= 0)
    return
      sameZone(zoneId, zones, zones[zd.subZone1]) 
      || sameZone(zoneId, zones, zones[zd.subZone2]);
  else
    return false;
}

static int checkContourForNeighbourZone(int *zoning, double *density,
					int Nx, int Ny, int Nz,
					ZoneVector& zones, ZoneDescriptor& zd)
{
  TreatmentList::iterator contourIterator, lastPixel;
  double minDensity = INFINITY;;
  int nextZone = -1;

  contourIterator = zd.contourPixels.begin();
  lastPixel = zd.contourPixels.end();
  while (contourIterator != lastPixel)
    {
      const DenXel& current = *contourIterator;
      int x = current.c[0];
      int y = current.c[1];
      int z = current.c[2];      

      for (int iz=-1;iz<=1;iz++)
	{
	  int Z = (z+Nz+iz) % Nz;
	  for (int iy=-1;iy<=1;iy++)
	    {
	      int Y = (y+Ny+iy) % Ny;
	      for (int ix=-1;ix<=1;ix++)
		{
		  int X = (x+Nz+ix) % Nx;
		  uint32_t idx = (Z*Ny + Y)*Nx + X;

		  if (zoning[idx] != NOZONE && !sameZone(zoning[idx], zones, zd) &&
		      density[idx] < minDensity)
		    {
		      minDensity = density[idx];
		      nextZone = zoning[idx];
		    }
		}
	    }
	}

      ++contourIterator;
    }

  return nextZone;
}

static double avgListLen;

template<typename T>
static void checkOrdering(list<T>& l)
{
  if (l.begin() == l.end())
    return;

  T* prev = &(*(l.begin()));
  typename list<T>::iterator i = l.begin();
  ++i;

  typename list<T>::iterator e = l.end();

  while (i != e)
    {
      T& p = *i;
      assert(*prev <= p);
      prev = &p;
      ++i;
    }
}

static bool insertNeighbours(ZoneDescriptor& zd,
		      DenXel& current, int32_t *zoning, 
		      double *density,
		      int Nx, int Ny, int Nz)
{
  uint32_t x = current.c[0];
  uint32_t y = current.c[1];
  uint32_t z = current.c[2];
  DenXel candidate;
  double d0 = current.d;
  TreatmentList& contour = zd.contourPixels;
  TreatmentList tmp_list;
  bool thisIsSaddle = false;

  for (int iz=-1;iz<=1;iz++)
    {
      int Z = (z+Nz+iz) % Nz;
      for (int iy=-1;iy<=1;iy++)
        {
          int Y = (y+Ny+iy) % Ny;
          for (int ix=-1;ix<=1;ix++)
            {
              int X = (x+Nz+ix) % Nx;
	              
              uint32_t idx = (Z*Ny + Y)*Nx + X;
              int zone = zoning[idx];
              double den = density[idx];
              
//              cout << format("Explore  (%d,%d,%d), zone=%d, %lg < den=%lg < %lg...") % X % Y % Z % zone % d0 % den % zd.maxDensity;
              if ((zone == NOZONE || zone == SADDLE) &&
                  den > d0 &&
                  den < zd.maxDensity)
                {
                  candidate.c[0] = X;
                  candidate.c[1] = Y;
                  candidate.c[2] = Z;
                  candidate.d = den;
                  assert(den > d0);
                  candidate.saddle = false;

                  tmp_list.insert(candidate);
                } 
              else 
                {
                    
              		if (den < d0 && (zone != SADDLE && zone != zd.zoneId))
              		  {
              		    thisIsSaddle = true;
        		      }
          	    }
            }
        }
    }
  if (!thisIsSaddle)
    contour.insert(tmp_list.begin(), tmp_list.end());
  return thisIsSaddle;
}


// -----------------------------------------------------------
// Do one iteration. We assume that zoneGrid is a grid filled
// with numbers indicating the belonging of a pixel to
// a minima.
// The returned value is the number of pixels that has newly
// assigned to a zone.
static uint64_t fillZones(int32_t *zoning,
                  double *density,
                  uint32_t Nx, uint32_t Ny, uint32_t Nz,
                  ZoneVector& zones, bool freeUpward)
{
  ZoneVector::iterator zi = zones.begin();
  uint64_t newlyZoned = 0;

  double localMin = INFINITY;
  while (zi != zones.end())
    {
      ZoneDescriptor& zd = *zi;
      
      if (zd.stopped)
        {
          ++zi;
          continue;
        }

      while (!zd.stopped)
      {
        // Check the surface density. We look for the pixel that is the minimum
        // in the exploration area. If we discover a saddle point 
        // (minimum is below the minimum of the surface) then we stop.
        // The contourPixels list is assumed to be ordered by density.
        // So, we start by the smallest density on the surface which is more
        // likely to be connected to a low density region.
        int currentZone = zd.zoneId;
        TreatmentList::iterator contourIterator = zd.contourPixels.begin();
        TreatmentList::iterator lastPixel = zd.contourPixels.end();
        

        // Contour iterator contains the list of candidates.
        // We pick the smallest one in this round, checking
        // that it has not already been zoned.
        // we add its neighbours to the list of candidates if they
        // satisfy the criterion.
        
        double localMinimum = INFINITY;
        DenXel chosenPixel;
        
        while (contourIterator != lastPixel)
          {
            const DenXel& p = *contourIterator;
            uint32_t idx = (p.c[2]*Ny + p.c[1]) * Nx + p.c[0];
            
            if ((zoning[idx] == NOZONE || zoning[idx]==SADDLE) && !p.saddle)
              break;
              
            if (p.saddle)
              zoning[idx] = SADDLE;
            
            zd.contourPixels.erase(contourIterator);
            contourIterator = zd.contourPixels.begin();
          }

        if (CHECK_ZONING)        
          avgListLen += zd.contourPixels.size();
        
        if (contourIterator == lastPixel)
          {
            // Mark the zone as having reached
            // the maximum extension as we have not
            // found any new DenXel candidate
            zd.stopped = true;
//            cout << "Stop" << endl;
            
            //	  mergeZone(zoning, density,
            //		    Nx, Ny, Nz,
            //		    zones, zd);
            newlyZoned++;
            continue;
          }
        
        // Now we have to grow the surface using this pixel.
        // Insert it at the right place.
        
        chosenPixel = *contourIterator;
        // Erase it from the list
        zd.contourPixels.erase(contourIterator);
        
        // We mark that the pixel has been assigned to a zone.
        uint32_t idx = chosenPixel.c[0] + Nx*(chosenPixel.c[1] + Ny*chosenPixel.c[2]);
        if (zoning[idx] != SADDLE)
            zoning[idx] = zd.zoneId;
        // Now add its neighbours, if they do not already
        // exist.
        bool saddle = insertNeighbours(zd, chosenPixel, zoning, density,
	           Nx, Ny, Nz);
        // This is a saddle point. Remove this pixel from the zone.
        if (saddle && freeUpward)
          zoning[idx] = SADDLE; 
        else if (saddle)
          zoning[idx] = NOZONE;
        newlyZoned++;
      }

      break;

   }

  return newlyZoned;
}

static uint32_t countActives(const ZoneVector& v)
{
  ZoneVector::const_iterator i = v.begin();
  ZoneVector::const_iterator last = v.end();
  uint32_t actives = 0;

  while (i != last)
    {
      if (!i->stopped)
        actives++;
      ++i;
    }
  return actives;
}

void divaWatershed(DivaDensity& density, DivaMinima& minima,
                   DivaZones& zoning,
                   boost::function1<void,const std::string&> progress)
{
  uint64_t Nvox = density.Nx*density.Ny*density.Nz;
  
  for (uint64_t i = 0; i < Nvox; i++)
    zoning.zoning[i] = NOZONE;

  ZoneVector zones;
  progress(str(format("Initialize seeds (maxGrowth=%lg)...") % zoning.maxDenGrowth));
  for (uint32_t i = 0; i < minima.numMins; i++)
    {
      ZoneDescriptor desc;
      
      desc.zoneId = i;
      desc.stopped = false;
      desc.maxDensity = zoning.maxDenGrowth;
      desc.firstTouchedZone = -1;
      desc.subZone1 = desc.subZone2 = -1;
      desc.superZone = -1;

      DenXel p;

      p.c[0] = minima.minTable[3*i+0];
      p.c[1] = minima.minTable[3*i+1];
      p.c[2] = minima.minTable[3*i+2];      
      p.saddle = false;

      uint32_t x = p.c[0];
      uint32_t y = p.c[1];
      uint32_t z = p.c[2];
      
      uint32_t idx = x + density.Nx*(y + density.Ny*z);

      zoning.zoning[idx] = i;
      p.d = desc.densityMinOnSurface = density.density[idx];
      insertNeighbours(desc, p, zoning.zoning, 
                       density.density, density.Nx, density.Ny, density.Nz);

      zones.push_back(desc);
    }

  progress("Finding zones...");
  uint64_t newPix;
  int cumInZone = 0;
  int iter = 0;
  int k =0;

  avgListLen = 0;
  while ((newPix = fillZones(zoning.zoning,
			     density.density,
			     density.Nx, density.Ny, density.Nz,
			     zones, zoning.freeUpward)) != 0)
    {
      if ((iter % 1) == 0)
        {
          progress(str(format("NumPix = %ld, Active zones = %ld, listLen = %ld") % newPix % countActives(zones) % (avgListLen/100)));
          avgListLen = 0;
        }
      iter++;
      k++;
    }

  progress("Done. Concluding...");

  zoning.contourz = new int8_t[Nvox];
  for (uint32_t i = 0; i < Nvox; i++)
    zoning.contourz[i] = -1;

  for (ZoneVector::iterator iter = zones.begin();
       iter != zones.end();
       ++iter)
    {
      TreatmentList& contour = iter->contourPixels;
      
      for (TreatmentList::iterator ip = contour.begin();
          ip != contour.end();
          ++ip)
      {
        uint32_t idx = (ip->c[2]*density.Ny + ip->c[1])*density.Nx + ip->c[0];

        zoning.contourz[idx] = 1;
      }
    }
        
  // We mark the surface
  for (uint32_t i = 0; i < zones.size(); i++)
    {
      uint32_t x = minima.minTable[3*i+0];
      uint32_t y = minima.minTable[3*i+1];
      uint32_t z = minima.minTable[3*i+2];
      
      uint32_t idx = x + density.Nx*(y + density.Ny*z);

      zoning.densityThresholds[3*i + 0] = zones[i].densityMinOnSurface;
      zoning.densityThresholds[3*i + 1] = zones[i].maxDensity;
      zoning.densityThresholds[3*i + 2] = density.density[idx];
    }
}
