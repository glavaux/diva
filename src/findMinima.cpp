#include <algorithm>
#include <iostream>
#include <cassert>
#include <list>
#include <cmath>
#include <boost/format.hpp>
#include "divaWatershed.hpp"

using boost::format;
using boost::str;

#define NUMDIMS 3

struct TestParticle {
  uint32_t idx;
};

static bool operator<(const TestParticle& p1, const TestParticle& p2)
{
  return p1.idx < p2.idx;
}

template<class T>
static void bubbleSort(std::list<T>& l)
{
  bool sorted = false;
  typename std::list<T>::iterator i, j, starter;

  int size = l.size();
  starter=l.begin();

  for(int pass=1; (pass<size)&&!sorted; ++pass,starter++)
    {
      i = j = starter;
      j++;
      sorted=true;
      for(; j!=l.end(); i++, j++)
        {
          //i=items.begin() or i=0
          if(*j<*i){//compare both values and swap if necessary
            std::swap(i, j);
          sorted=false;
        }
    }
  }
}//end bubble sort


//typedef std::list<TestParticle> StateList;
typedef TestParticle *StateList;

using namespace std;

static bool sameIndex(const TestParticle& p1, const TestParticle& p2)
{
  return p1.idx == p2.idx;
}

static void computeXYZ(uint32_t idx, uint32_t& x, uint32_t& y, uint32_t& z, uint32_t *dimList)
{
  x = idx % dimList[0];
  y = (idx/dimList[0]) % dimList[1];
  z = idx/(dimList[0]*dimList[1]);
}

static void unique(TestParticle *init, uint32_t& stateLen)
{
  TestParticle *cur = init;
  TestParticle *p = init;
  TestParticle *end = init+stateLen;

  do
    {
      ++p;
      if (!sameIndex(*cur, *p))
        {
          ++cur;
          if (cur != p)
            memcpy(cur, p, sizeof(TestParticle));
        }
    }
  while (p != end);

  stateLen = cur-init;
}

template<typename T, typename F>
static uint32_t doMotion(T *densityField, uint32_t *dimList, StateList& state, uint32_t& stateLen, F& progress)
{
//  TestParticle *iter = state;
//  TestParticle *end = state+stateLen;

  uint32_t Nx = dimList[0], Ny = dimList[1], Nz = dimList[2];
  uint32_t numMove = 0;

#pragma omp parallel for schedule(static) reduction(+:numMove)
  for (uint32_t p = 0; p < stateLen; p++) 
    {
      TestParticle *iter = &state[p];
      double dZ_0, dZ_1, dY_0, dY_1, dX_0, dX_1;
      uint32_t idx_Z_0, idx_Z_1, idx_Y_0, idx_Y_1, idx_X_0, idx_X_1;
      
      uint32_t x;
      uint32_t y;
      uint32_t z;
      uint32_t idx = iter->idx;
      computeXYZ(idx, x, y, z, dimList);

      T d0 = densityField[idx];

      int moved = 0;
        
      bool found = false;
      uint32_t minIDX = idx;
      int minX = x, minY = y, minZ = z;
      for (int iz=-1;iz<=1;iz++)
        {
          int Z = (z+Nz+iz) % Nz;
          for (int iy=-1;iy<=1;iy++)
          {
            int Y = (y+Ny+iy) % Ny;
            for (int ix=-1;ix<=1;ix++)
              {
                int X = (x+Nz+ix) % Nx;

                idx = (Z*Ny + Y)*Nx + X;
                if (densityField[idx] < d0)
                  {
                    minX = X; minY = Y; minZ = Z;
                    minIDX = idx;
                    moved = 1;
                  }
              }
          }
        }      

      iter->idx = minIDX;
      numMove += moved;
    }
  
  // Now reduce redundancies.
  //  state.sort();
  //  state.sort();
  //  state.unique(sameIndex);
  progress("Sorting...");
  sort(state, state+stateLen);
  progress("Removing duplicates...");
  unique(state, stateLen);

  return numMove;
}


void divaFindMinima(DivaDensity& density, std::vector<DivaMinimumInt>& minima,
                    boost::function1<void,const std::string&> progress)
{
  StateList currentState;
  uint32_t stateLen = density.Nz*density.Ny*density.Nx;

  currentState = new TestParticle[stateLen];

  progress("Initialization");
  for (uint32_t iz = 0; iz < density.Nz; iz++)
    {
      for (uint32_t iy = 0; iy < density.Ny; iy++)
        {
          for (uint32_t ix = 0; ix < density.Nx; ix++)
            {
              TestParticle p = { iz*density.Ny*density.Nx + iy*density.Nx + ix };
              currentState[p.idx] = p;
            }	  
        }
    }

  progress("Sorting ids...");
  sort(currentState, currentState + stateLen);

  progress("Moving...");
  bool actionWasDone = false;
  uint32_t dimList[3] = { density.Nx, density.Ny, density.Nz };
  do
    {
      uint32_t numMove;
      numMove = doMotion(density.density, dimList, currentState, stateLen, progress);
      actionWasDone = (numMove > 0);
      progress(str(format("Iteration... (moved=%ld)") % numMove));
    }
  while (actionWasDone);

  for (uint32_t i = 0; i < stateLen; i++)
    {
      DivaMinimumInt m;  

      computeXYZ(currentState[i].idx, m.x, m.y, m.z, dimList);
      minima.push_back(m);
    }

  delete[] currentState;
}
