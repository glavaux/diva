#ifndef __DIVA_WATERSHED_HPP
#define __DIVA_WATERSHED_HPP

#include <stdint.h>
#include <vector>
#include <boost/function.hpp>

struct DivaDensity {
  double *density;
  uint64_t Nx, Ny, Nz;
};

struct DivaMinima {
  uint32_t *minTable; 
  uint32_t numMins;
};

struct DivaZones {
  double maxDenGrowth;
  bool freeUpward;
  int32_t *zoning; // Must be Nvox
  int8_t *contourz; // Must be Nvox
  double *densityThresholds; // Must be 3 * numMins
};

struct DivaMinimumInt {
  uint32_t x, y, z;
};

void divaWatershed(DivaDensity& density, DivaMinima& minima,
                   DivaZones& zones,
                   boost::function1<void,const std::string&> progress);

void divaFindMinima(DivaDensity& density, std::vector<DivaMinimumInt>& minima,
                    boost::function1<void,const std::string&> progress);
void divaFindMinima2(DivaDensity& density, std::vector<DivaMinimumInt>& minima,
                    boost::function1<void,const std::string&> progress);

void divaWatershed2(DivaDensity& density, std::vector<DivaMinimumInt>& minima,
                    DivaZones& zones,
                    boost::function1<void,const std::string&> progress);


#endif
