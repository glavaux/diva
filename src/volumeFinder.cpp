#include <cmath>
#include <cassert>
#include <vector>
#include <list>
#include <iostream>
#include <fstream>
#include "miniargs.hpp"
#include "yorick.hpp"

using namespace HoyleVogeley;
using namespace std;

typedef struct {
  int c[3];
} IntCoord;

typedef struct {
  int c[3];
  double d;
} DenXel;

typedef std::list<DenXel> TreatmentList;
typedef std::vector<IntCoord> CoordVector;

static const int NOZONE =  -1;

typedef struct {
  int zoneId;
  bool stopped;
  double densityMinOnSurface;
  double maxDensity;
  int firstTouchedZone;
  uint32_t touchPixel;
  TreatmentList contourPixels;
  int subZone1, subZone2;
  int superZone;
} ZoneDescriptor;

typedef std::vector<ZoneDescriptor> ZoneVector;

bool operator<(const DenXel& a, const DenXel& b)
{
  return a.d < b.d;
}

// -------------------------------------------------------
// Load the minima table (3 columns, N lines). It is pure
// text file. We throw NoSuchFileException if
// the file does not exist.
CoordVector loadMinTable(const char *fname)
  throw (NoSuchFileException)
{
  ifstream f(fname);
  CoordVector v;
  IntCoord ic;
  
  if (!f)
    throw NoSuchFileException();


  while (!(f >> ic.c[0] >> ic.c[1] >> ic.c[2]).eof())
    v.push_back(ic);

  return v;
}
// --------------------------------------------------------


static bool sameZone(int zoneId, const ZoneVector& zones, const ZoneDescriptor& zd)
{
  // Either we are the zone
  if (zoneId == zd.zoneId)
    return true;

  // Or it is one the subzone
  if (zd.subZone1 >= 0)
    return
      sameZone(zoneId, zones, zones[zd.subZone1]) 
      || sameZone(zoneId, zones, zones[zd.subZone2]);
  else
    return false;
}

static int checkContourForNeighbourZone(int *zoning, double *density,
					int Nx, int Ny, int Nz,
					ZoneVector& zones, ZoneDescriptor& zd)
{
  TreatmentList::iterator contourIterator, lastPixel;
  double minDensity = INFINITY;;
  int nextZone = -1;

  contourIterator = zd.contourPixels.begin();
  lastPixel = zd.contourPixels.end();
  while (contourIterator != lastPixel)
    {
      DenXel& current = *contourIterator;
      int x = current.c[0];
      int y = current.c[1];
      int z = current.c[2];      

      for (int iz=-1;iz<=1;iz++)
	{
	  int Z = (z+Nz+iz) % Nz;
	  for (int iy=-1;iy<=1;iy++)
	    {
	      int Y = (y+Ny+iy) % Ny;
	      for (int ix=-1;ix<=1;ix++)
		{
		  int X = (x+Nz+ix) % Nx;
		  uint32_t idx = (Z*Ny + Y)*Nx + X;

		  if (zoning[idx] != NOZONE && !sameZone(zoning[idx], zones, zd) &&
		      density[idx] < minDensity)
		    {
		      minDensity = density[idx];
		      nextZone = zoning[idx];
		    }
		}
	    }
	}

      ++contourIterator;
    }

  return nextZone;
}

static void mergeZone(int *zoning, double *density,
		      int Nx, int Ny, int Nz,
		      ZoneVector& zones, ZoneDescriptor& zd)
{
  ZoneDescriptor newZone;

  cerr << zd.zoneId << " has now stopped. Trying to merge." << endl;

  // If no zone were touched. We have to really check that the contour is touching nothing.
  if (zd.firstTouchedZone < 0)
    {
      zd.firstTouchedZone = checkContourForNeighbourZone(zoning, density,
							 Nx, Ny, Nz, zones, zd);
      assert(zd.firstTouchedZone >= 0);
    }
  int firstTouched = zd.firstTouchedZone;
  ZoneDescriptor& secondZone = zones[firstTouched];
  // First check whether our informations are up-to-date. Has the "firstTouchedZone"
  // merged with somebody else ?
  while (secondZone.superZone >= 0)
    {
      // Move to the super-zone
      secondZone = zones[secondZone.superZone];
    }
  
  cerr << "     First touched was: " << firstTouched << endl;
  cerr << "     We found a candidate: " << secondZone.zoneId << endl;

  // It may be that the secondZone is not yet stopped (particularly after merging)
  // Let's stop it first.
  if (!secondZone.stopped)
    secondZone.stopped = true;

  // We touched a zone. By construction this zone is limiting us for the
  // growth. We produce a new zone that corresponds to the two zones.
  newZone.zoneId = zones.size();
  newZone.stopped = false;
  newZone.densityMinOnSurface = min(secondZone.densityMinOnSurface, zd.densityMinOnSurface);
  // The second zone were stopped by another one. Use this new max density.
  // In the other case, this second would already have merged with another one.
  newZone.maxDensity = secondZone.maxDensity;
  newZone.firstTouchedZone = secondZone.firstTouchedZone;
  newZone.subZone1 = zd.zoneId;
  newZone.subZone2 = secondZone.zoneId;
  zd.superZone = newZone.zoneId;  

  newZone.contourPixels.merge(zd.contourPixels);
  newZone.contourPixels.merge(secondZone.contourPixels);


  cerr << "     We built a new zone : " << newZone.zoneId << endl;

  // Introduce the new zone in the vector
  zones.push_back(newZone);
}

static void checkMaxDensityThreshold(int *zoning, double *density, ZoneDescriptor& zd,
				     const ZoneVector& zones,
				     DenXel& current,
				     int Nx, int Ny, int Nz)
{
  double& maxDen = zd.maxDensity;
  double d0 = current.d;
  int x = current.c[0];
  int y = current.c[1];
  int z = current.c[2];

  for (int iz=-1;iz<=1;iz++)
    {
      int Z = (z+Nz+iz) % Nz;
      for (int iy=-1;iy<=1;iy++)
	{
	  int Y = (y+Ny+iy) % Ny;
	  for (int ix=-1;ix<=1;ix++)
	    {
	      int X = (x+Nz+ix) % Nx;
		      
	      uint32_t idx = (Z*Ny + Y)*Nx + X;
	      int zone = zoning[idx];
	      double den = density[idx];
	      
	      // We reached a saddle point. Stop the growth
	      if ((zone != NOZONE && !sameZone(zone, zones, zd)) ||
		  (zone == NOZONE && den < d0))
		{
		  if (maxDen > den)
		    {
		      maxDen = den;
		      zd.touchPixel = idx;
		      zd.firstTouchedZone = zone;
		    }
		}
	    }
	}
    }
}
  
static int checkBestDenXel(int *zoning, double *density, ZoneDescriptor& zd, const ZoneVector& zones,
			   DenXel& current, DenXel& chosenPixel, double& localMinimum,
			   int Nx, int Ny, int Nz)
{
  int numNeighbour = 0;
  int currentZone = zd.zoneId;
  double d0 = current.d;
	  
  uint32_t x = current.c[0];
  uint32_t y = current.c[1];
  uint32_t z = current.c[2];

  for (int iz=-1;iz<=1;iz++)
    {
      int Z = (z+Nz+iz) % Nz;
      for (int iy=-1;iy<=1;iy++)
	{
	  int Y = (y+Ny+iy) % Ny;
	  for (int ix=-1;ix<=1;ix++)
	    {
	      int X = (x+Nz+ix) % Nx;
		      
	      uint32_t idx = (Z*Ny + Y)*Nx + X;
	      int zone = zoning[idx];
	      double den = density[idx];
	      
	      if (zone == NOZONE &&
		  den > d0 &&
		  den < localMinimum &&
		  den < zd.maxDensity)
		{
		  chosenPixel.c[0] = X;
		  chosenPixel.c[1] = Y;
		  chosenPixel.c[2] = Z;
		  chosenPixel.d = density[idx];
		  localMinimum = density[idx];
		}
	      if (sameZone(zoning[idx], zones, zd))
		numNeighbour++;
	    }
	}
    }

  return numNeighbour;
}

// -----------------------------------------------------------
// Do one iteration. We assume that zoneGrid is a grid filled
// with numbers indicating the belonging of a pixel to
// a minima.
// The returned value is the number of pixels that has newly
// assigned to a zone.
int fillZones(int *zoning,
	      double *density,
	      uint32_t Nx, uint32_t Ny, uint32_t Nz,
	      ZoneVector& zones)
{
  ZoneVector::iterator zi = zones.begin();
  int newlyZoned = 0;

  while (zi != zones.end())
    {
      ZoneDescriptor& zd = *zi;
      if (zd.stopped)
	{
	  ++zi;
	  continue;
	}

      // Check the surface density. We look for the pixel that is the minimum
      // in the exploration area. If we discover a saddle point 
      // (minimum is below the minimum of the surface) then we stop.
      // The contourPixels list is assumed to be ordered by density.
      // So, we start by the smallest density on the surface which more
      // likely to be connected to a low density region.
      int currentZone = zd.zoneId;
      TreatmentList::iterator contourIterator = zd.contourPixels.begin();
      TreatmentList::iterator lastPixel = zd.contourPixels.end();

      // Check whether we are crashing into another zone.
      while (contourIterator != lastPixel)
	{
	  DenXel& p = *contourIterator;

	  checkMaxDensityThreshold(zoning, density, zd, zones,
				   p, Nx, Ny, Nz);
	  ++contourIterator;
	}
      
      double localMinimum = INFINITY;
      DenXel chosenPixel;
      contourIterator = zd.contourPixels.begin();
      while (contourIterator != lastPixel)
	{
	  int numNeighbour = 0;
	  DenXel& p = *contourIterator;

	  // We have already found an adequate minimum, break now the loop.
	  if (localMinimum < p.d)
	    break;

	  numNeighbour = checkBestDenXel(zoning, density, zd, zones,
					 p, chosenPixel, localMinimum,
					 Nx, Ny, Nz);
	  
	  //	  assert(localMinimum > zd.densityMinOnSurface);

	  if (numNeighbour == 27)
	    {
	      //	      cout << "Kill pixel " << endl;
	      // Go to the next after killing this node.
	      assert(contourIterator != lastPixel);
	      contourIterator = zd.contourPixels.erase(contourIterator);	      
	      // We killed the DenXel with the smallest density.
	      if (contourIterator == zd.contourPixels.begin())
		zd.densityMinOnSurface = contourIterator->d;
	      continue;
	    }
	  assert(numNeighbour < 27);
	  ++contourIterator;
	}
      
      assert(localMinimum >= zd.densityMinOnSurface);
      if (localMinimum == INFINITY)
	{
	  // Mark the zone as having reached
	  // the maximum extension as we have not
	  // found any new DenXel candidate
	  zd.stopped = true;
	  //	  mergeZone(zoning, density,
	  //		    Nx, Ny, Nz,
	  //		    zones, zd);
	  newlyZoned++;
	  ++zi;
	  continue;
	}
      assert(localMinimum <= zd.maxDensity);

      // Now we have to grow the surface using this pixel.
      // Insert it at the right place.
      
      contourIterator = zd.contourPixels.begin();
      lastPixel = zd.contourPixels.end();

      double previousD = -INFINITY;

      // ====
      // Check consitency
      while (contourIterator != lastPixel)
	{
	  DenXel& p = *contourIterator;
	  assert(previousD <= p.d);
	  previousD = p.d;
	  ++contourIterator;
	}
      // ====

      contourIterator = zd.contourPixels.begin();
      while (contourIterator != lastPixel)
	{
	  DenXel& p = *contourIterator;
	  uint32_t x = p.c[0];
	  uint32_t y = p.c[1];
	  uint32_t z = p.c[2];

	  double d0 = p.d;
	  if (d0 > localMinimum)
	    {
	      zd.contourPixels.insert(contourIterator, chosenPixel);
	      break;
	    }

	  ++contourIterator;
	}
      if (contourIterator == lastPixel)
	{
	  zd.contourPixels.insert(lastPixel, chosenPixel);	  
	}

      // We introduced a DenXel with the smallest density on the surface.
      if (zd.densityMinOnSurface > localMinimum)
	zd.densityMinOnSurface = localMinimum;
      // We mark that the pixel has been assigned to a zone.
      uint32_t idx = chosenPixel.c[0] + Nx*(chosenPixel.c[1] + Ny*chosenPixel.c[2]);
      zoning[idx] = zd.zoneId;
      newlyZoned++;
      ++zi;
   }

  return newlyZoned;
}

int main(int argc, char **argv)
{
  double *density;
  uint32_t *dimList, dimRank;
  char *inName, *minName;
  CoordVector minTable;
  MiniArgDesc desc[] = {
    { "DENSITY FILE", &inName, MINIARG_STRING },
    { "MINIMA FILE", &minName, MINIARG_STRING },
    { 0, 0, MINIARG_NULL }
  };

  if (!parseMiniArgs(argc, argv, desc))
    return 1;

  try
    {
      loadDoubleArray(inName,
		      density, dimList, dimRank);
    }
  catch (const NoSuchFileException& e)
    {
      cerr << "No such file " << inName << endl;
      return 1;
    }

  if (dimRank != 3)
    {
      cerr << "Invalid grid rank " << dimRank << endl;
      return 2;
    }

  uint32_t Nz = dimList[0], Ny = dimList[1], Nx = dimList[2];
  cout << "Got a density grid " << Nx << "x" << Ny << "x" << Nz << endl;

  try
    {
      minTable = loadMinTable(minName);
    }  
  catch (const NoSuchFileException& e)
    {
      cerr << "No such file " << inName << endl;
      return 1;
    }
  cout << "Got " << minTable.size() << " minima to explore" << endl;
  cout << "Initialize variables..." << endl;

  int *zoning = new int[Nx*Ny*Nz];
  for (int i = 0; i < Nz*Ny*Nz; i++)
    zoning[i] = NOZONE;


  ZoneVector zones;
  for (int i = 0; i < minTable.size(); i++)
    {
      ZoneDescriptor desc;
      
      desc.zoneId = i;
      desc.stopped = false;
      desc.maxDensity = INFINITY;
      desc.firstTouchedZone = -1;
      desc.subZone1 = desc.subZone2 = -1;
      desc.superZone = -1;

      DenXel p;

      p.c[0] = minTable[i].c[0];
      p.c[1] = minTable[i].c[1];
      p.c[2] = minTable[i].c[2];      

      uint32_t x = p.c[0];
      uint32_t y = p.c[1];
      uint32_t z = p.c[2];
      
      uint32_t idx = x + Nx*(y + Ny*z);

      zoning[idx] = i;
      p.d = desc.densityMinOnSurface = density[idx];
      desc.contourPixels.push_back(p);
      zones.push_back(desc);
    }

  cout << "Finding zones..." << endl;
  int newPix;
  int cumInZone = 0;
  int iter = 0;
  while ((newPix = fillZones(zoning,
			     density,
			     Nx, Ny, Nz,
			     zones)) != 0)
    {
      if ((iter % 1000) == 0)
	cout << "NumPix = " << newPix << endl;
      iter++;

    }

  double *densityThresholds = new double[3*zones.size()];
  // We mark the surface
  for (uint32_t i = 0; i < zones.size(); i++)
    {
//      cout << "Zone " << i << " min=" << zones[i].densityMinOnSurface << " max=" << zones[i].maxDensity << endl;

      uint32_t x = minTable[i].c[0];
      uint32_t y = minTable[i].c[1];
      uint32_t z = minTable[i].c[2];
      
      uint32_t idx = x + Nx*(y + Ny*z);

      densityThresholds[3*i + 0] = zones[i].densityMinOnSurface;
      densityThresholds[3*i + 1] = zones[i].maxDensity;
      densityThresholds[3*i + 2] = density[idx];

#if 0
      TreatmentList::iterator b = zones[i].contourPixels.begin();
      TreatmentList::iterator e = zones[i].contourPixels.end();

      while (b != e)
	{
	  uint32_t x = b->c[0];
	  uint32_t y = b->c[1];
	  uint32_t z = b->c[2];
	  uint32_t idx = x + Nx*(y + Ny*z);

	  zoning[idx] = 10*(i+1);
	  ++b;
	}
#endif
    }
  
  uint32_t thrDims[] = { minTable.size(), 3 };
  
  saveIntArray("zones.nc", zoning, dimList, dimRank);
  saveDoubleArray("zonesThresholds.nc", densityThresholds, thrDims, 2);

  return 0;
}
