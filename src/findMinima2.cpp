#include <algorithm>
#include <iostream>
#include <cassert>
#include <list>
#include <cmath>
#include <boost/format.hpp>
#include "divaWatershed.hpp"

using boost::format;
using boost::str;

#define NUMDIMS 3

struct TestParticle {
  uint32_t idx;
};

static bool operator<(const TestParticle& p1, const TestParticle& p2)
{
  return p1.idx < p2.idx;
}

template<class T>
static void bubbleSort(std::list<T>& l)
{
  bool sorted = false;
  typename std::list<T>::iterator i, j, starter;

  int size = l.size();
  starter=l.begin();

  for(int pass=1; (pass<size)&&!sorted; ++pass,starter++)
    {
      i = j = starter;
      j++;
      sorted=true;
      for(; j!=l.end(); i++, j++)
        {
          //i=items.begin() or i=0
          if(*j<*i){//compare both values and swap if necessary
            std::swap(i, j);
          sorted=false;
        }
    }
  }
}//end bubble sort


//typedef std::list<TestParticle> StateList;
typedef TestParticle *StateList;

using namespace std;

static bool sameIndex(const TestParticle& p1, const TestParticle& p2)
{
  return p1.idx == p2.idx;
}

static void computeXYZ(uint32_t idx, uint32_t& x, uint32_t& y, uint32_t& z, uint32_t *dimList)
{
  x = idx % dimList[0];
  y = (idx/dimList[0]) % dimList[1];
  z = idx/(dimList[0]*dimList[1]);
}

static void unique(TestParticle *init, uint32_t& stateLen)
{
  TestParticle *cur = init;
  TestParticle *p = init;
  TestParticle *end = init+stateLen;

  do
    {
      ++p;
      if (!sameIndex(*cur, *p))
        {
          ++cur;
          if (cur != p)
            memcpy(cur, p, sizeof(TestParticle));
        }
    }
  while (p != end);

  stateLen = cur-init;
}

template<typename T, typename F>
static uint32_t doMotion(T *densityField, uint32_t *dimList, std::vector<DivaMinimumInt>& minima, F& progress)
{
//  TestParticle *iter = state;
//  TestParticle *end = state+stateLen;

  uint64_t Nx = dimList[0], Ny = dimList[1], Nz = dimList[2];
  uint64_t Nvox = Nx*Ny*Nz;
  uint32_t numMove = 0;

#pragma omp parallel for schedule(static) reduction(+:numMove)
  for (uint32_t p = 0; p < Nvox; p++) 
    {
      double dZ_0, dZ_1, dY_0, dY_1, dX_0, dX_1;
      uint32_t idx_Z_0, idx_Z_1, idx_Y_0, idx_Y_1, idx_X_0, idx_X_1;
      
      uint32_t x;
      uint32_t y;
      uint32_t z;
      uint32_t idx = p;
      computeXYZ(idx, x, y, z, dimList);

      T d0 = densityField[idx];

      int moved = 0;
        
      bool found = false;
      uint32_t minIDX = idx;
      int minX = x, minY = y, minZ = z;
      for (int iz=-1;iz<=1;iz++)
        {
          int Z = (z+Nz+iz) % Nz;
          for (int iy=-1;iy<=1;iy++)
          {
            int Y = (y+Ny+iy) % Ny;
            for (int ix=-1;ix<=1;ix++)
              {
                int X = (x+Nz+ix) % Nx;

                idx = (Z*Ny + Y)*Nx + X;
                if (densityField[idx] < d0)
                  {
                    minX = X; minY = Y; minZ = Z;
                    minIDX = idx;
                    moved = 1;
                  }
              }
          }
        }      

      if (!moved) {
        DivaMinimumInt m;  
        m.x = x;
        m.y = y;
        m.z = z;
#pragma omp critical
        minima.push_back(m);
      }
      numMove += moved;
    }
  
  return numMove;
}


void divaFindMinima2(DivaDensity& density, std::vector<DivaMinimumInt>& minima,
                    boost::function1<void,const std::string&> progress)
{
  StateList currentState;
  uint32_t dimList[] = { density.Nx, density.Ny, density.Nz };
  minima.clear();
  doMotion(density.density, dimList, minima, progress);
}
