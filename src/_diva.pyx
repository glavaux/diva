cimport numpy as np
import numpy as np
from libcpp cimport string as cppstring
from libcpp cimport vector as cppvector
from libcpp cimport list as cpplist
from cython cimport view
from libcpp cimport bool
cimport libcpp.list
from cython.operator cimport dereference as deref, preincrement as inc


cdef extern from "matcher.hpp" namespace "DIVA":

  ctypedef libcpp.list.list[np.int32_t] divamatch_list

  cdef cppclass DivaMatch_int:
    cppvector.vector[libcpp.list.list[np.int32_t] ] watershed_tree
    cppvector.vector[np.int32_t] watershed_match
    float matchThreshold
    float mergeThreshold

  void divaMatcher "DIVA::divaMatcher<int32_t *,void (*)(const std::string&), int32_t>" (np.int32_t *zone1, np.int32_t *zone1_end, np.int32_t *zone2, np.int32_t *zone2_end, np.uint32_t, np.uint32_t, void *progress, DivaMatch_int& match)

cdef extern from "divaWatershed.hpp":

  struct DivaDensity:
    double *density
    np.uint64_t Nx
    np.uint64_t Ny
    np.uint64_t Nz

  struct DivaMinima:
    np.uint32_t *minTable
    np.uint32_t numMins
    
  struct DivaZones:
    double maxDenGrowth
    bool freeUpward
    np.int32_t *zoning
    np.int8_t *contourz
    double *densityThresholds

  struct DivaMinimumInt:
    np.uint32_t x
    np.uint32_t y
    np.uint32_t z
    
  void divaWatershed(DivaDensity& density, DivaMinima& minima,
                     DivaZones& zones,
                     void *progress) nogil

  void divaFindMinima(DivaDensity& density, cppvector.vector[DivaMinimumInt]& minima,
                      void * progress) nogil

  void divaFindMinima2(DivaDensity& density, cppvector.vector[DivaMinimumInt]& minima,
                      void * progress) nogil

  void divaWatershed2(DivaDensity& density, cppvector.vector[DivaMinimumInt]& minima,
                    DivaZones& zones,
                    void *progress) nogil

cdef void progress(const cppstring.string& a) nogil:

  with gil:
    print(a.c_str())

cdef void quiet_progress(const cppstring.string& s) nogil:
  pass


class Watershed:
  """
  Watershed object describing a Watershed run on a regular mesh.
"""

  def __init__(self,minima,zoning,contour,thresholds,maxDensity,minDensity):
    self.minima = minima
    self.zoning = zoning
    self.contour = contour
    self.thresholds = thresholds
    self.maxDensity = maxDensity
    self.minDensity = minDensity
    

def divaZones(double[:,:,::1] density, bool quiet=False, double maxDenGrowth=0):
  """
  divaZones(double[:,:,::1] density, bool quiet=False, double maxDenGrowth=0)

  density is a 3d array with the density field to compute the watershed from.
  If quiet is not True, the code will print some progress information.
  maxDenGrowth tell what should be the absolute wall of voids. By default it is the mean density.

  It returns a Watershed object that can be used by divaTree.
"""
  cdef DivaDensity dd
  cdef DivaZones zones
  cdef cppvector.vector[DivaMinimumInt] minima
  cdef np.uint32_t[:,::1] minTable
  cdef np.int32_t[:,:,:] zoning_array
  
  dd.density = &density[0,0,0]
  dd.Nx = density.shape[2]
  dd.Ny = density.shape[1]
  dd.Nz = density.shape[0]

  zoning_array = view.array(shape=(density.shape[0],density.shape[1],density.shape[2]), itemsize=sizeof(np.int32_t), format="i")

  zones.maxDenGrowth = maxDenGrowth
  zones.zoning = &zoning_array[0,0,0]

  with nogil:
    divaWatershed2(dd, minima, zones, &progress if not quiet else &quiet_progress)
 
  minTable = view.array(shape=(minima.size(), 3),itemsize=sizeof(np.uint32_t), format="I")
  for i in xrange(minima.size()):
    minTable[i,0] = minima[i].x
    minTable[i,1] = minima[i].y
    minTable[i,2] = minima[i].z

  return Watershed(minTable, zoning_array,None,None,None,None)
  
def divaTree(object w1, object w2, double matchThreshold=0.5, double mergeThreshold=0.3, quiet=False):
  """
  divaTree(object w1, object w2, double matchThreshold=0.5, double mergeThreshold=0.3)
  
  Compute the tree relation between the watershed w1 and w2. It assumes
  that w1 is 'bigger' than w2 (that is all zones of w2 are includes in zones of w1).

  matchThreshold tells what mass fraction is required to match two zones between w1 and w2.
  mergeThrehsold is the mass fraction for a merge (instead of accrete).
"""

  cdef np.int32_t[:,:,::1] zone1
  cdef np.int32_t[:,:,::1] zone2
  cdef np.uint32_t[:,:] minTable1, minTable2
  cdef np.uint64_t iz, iy, ix, idx, s1, s2
  cdef np.uint64_t Nx, Ny, Nz
  cdef np.uint32_t numZ1, numZ2
  cdef list minSet1, minSet2
  cdef np.ndarray[ndim=2, dtype=np.uint64_t] inter_s_len
  cdef set inter_s
  cdef np.ndarray[ndim=1, dtype=np.float32_t] ratio
  cdef dict watershed_match
  cdef dict watershed_tree
  cdef object NoArray
  cdef void (*myprogress)(const cppstring.string& a) nogil

  if not isinstance(w1, Watershed) or not isinstance(w2, Watershed):
    raise ValueError("w1 and w2 must be watersheds")

  myprogress = &quiet_progress if quiet else &progress
  
  zone1 = w1.zoning
  zone2 = w2.zoning
  
  minTable1 = w1.minima
  minTable2 = w2.minima
  numZ1 = minTable1.shape[0]
  numZ2 = minTable2.shape[0]
  minSet1 = []
  minSet2 = []
  
  myprogress("Initializing set for w1")
  for i1 in xrange(numZ1):
    minSet1.append(set())
  for i2 in xrange(numZ2):
    minSet2.append(set())
  
  myprogress("Adding voxels to sets")
  Nz = zone1.shape[0]
  Ny = zone1.shape[1]
  Nx = zone1.shape[2]
  for iz in xrange(Nz):
    for iy in xrange(Ny):
      for ix in xrange(Nx):
        idx = ix + Nx*(iy + Ny*iz)
        if zone1[iz,iy,ix] >= 0:
          minSet1[zone1[iz,iy,ix]].add(idx)
        if zone2[iz,iy,ix] >= 0:
          minSet2[zone2[iz,iy,ix]].add(idx)
  
  watershed_match = dict()
  watershed_tree = dict()
  
  myprogress("Computing intersections")
  NoArray = np.empty(0,dtype=np.uint64)
  inter_s_len = np.ndarray(shape=(numZ1, numZ2), dtype=np.uint64)
  for s1 in xrange(numZ1):
    if (s1 % 10) == 0:
      myprogress(" Void %d / %d" % (s1, numZ1))
      
    for s2 in range(numZ2):
      inter_s = minSet1[s1].intersection(minSet2[s2])
      inter_s_len[s1,s2] = len(inter_s)
      
    ratio = inter_s_len[s1,:].astype(np.float32)/float(len(minSet1[s1]))
    id_merge = NoArray
    ref_void = ratio[:].argmax()
    if ratio[ref_void] < matchThreshold:
      watershed_match[s1] = None
    else:
      watershed_match[s1] = ref_void
      id_merge = np.where((ratio>mergeThreshold))[0]
#      id_merge = id_merge[id_merge!=ref_void]
    watershed_tree[s1] = id_merge
    
  return watershed_match,watershed_tree,inter_s_len
  
  
  
def divaTree2(object w1, object w2, double matchThreshold=0.5, double mergeThreshold=0.3, quiet=False, nodict=False):
  """
  divaTree2(object w1, object w2, double matchThreshold=0.5, double mergeThreshold=0.3)
  
  Compute the tree relation between the watershed w1 and w2. It assumes
  that w1 is 'bigger' than w2 (that is all zones of w2 are includes in zones of w1).

  matchThreshold tells what mass fraction is required to match two zones between w1 and w2.
  mergeThrehsold is the mass fraction for a merge (instead of accrete).
"""
  cdef void (*myprogress)(const cppstring.string& a) nogil
  cdef np.int32_t[:,:,::1] zone1
  cdef np.int32_t[:,:,::1] zone2
  cdef np.uint32_t[:,:] minTable1, minTable2
  cdef np.uint32_t iZ1
  cdef np.uint32_t iZ2
  cdef np.uint64_t sz1, sz2
  cdef DivaMatch_int match
  cdef int i
  cdef np.int32_t a
  cdef libcpp.list.list[np.int32_t]& l
  cdef libcpp.list.list[np.int32_t].iterator l_iter
  cdef libcpp.list.list[np.int32_t].iterator l_end
  cdef np.ndarray[ndim=1,dtype=np.int32_t] watershed_match_arr
  cdef object t

  if not isinstance(w1, Watershed) or not isinstance(w2, Watershed):
    raise ValueError("w1 and w2 must be watersheds")

  myprogress = &quiet_progress if quiet else &progress
  
  zone1 = w1.zoning
  zone2 = w2.zoning
  
  minTable1 = w1.minima
  minTable2 = w2.minima
  iZ1 = minTable1.shape[0]
  iZ2 = minTable2.shape[0]
  sz1 = zone1.size
  sz2 = zone2.size

  divaMatcher(&zone1[0,0,0], &zone1[0,0,0] + sz1, 
              &zone2[0,0,0], &zone2[0,0,0] + sz2, iZ1, iZ2, myprogress, match)

  if not nodict:
    watershed_match = dict()
    watershed_tree = dict()
  
    for i in xrange(match.watershed_match.size()):
      watershed_match[i] = match.watershed_match[i]
      l_iter = match.watershed_tree[i].begin()
      l_end = match.watershed_tree[i].end()
      t = []
      while l_iter != l_end:
        a = deref(l_iter)
        t.append(a)
        inc(l_iter)
      watershed_tree[i] = t

    return watershed_match,watershed_tree,0
  else:
    watershed_match_arr = np.empty((match.watershed_match.size(), ), dtype=np.int32)
    for i in xrange(match.watershed_match.size()):
      watershed_match_arr[i] = match.watershed_match[i]

    return watershed_match_arr

def divaZoneRewrite(np.int32_t[:,:,::1] zones, np.int32_t[:] matched):
  """
  divaZoneRewrite(np.int32_t[:,:,::1] zones, np.int32_t[:] matched)

  rewrite the id in zones according to the matched array. If the zone is overflowing the matched
  array, then a -1 is put in place
"""
  cdef np.int32_t z
  cdef np.uint64_t p

  for px in xrange(zones.shape[0]):
    for py in xrange(zones.shape[1]):
      for pz in xrange(zones.shape[2]):
        z = zones[px,py,pz]
        if z >= matched.size or z < 0:
          zones[px,py,pz] = -1
        else:
          zones[px,py,pz] = matched[z]
