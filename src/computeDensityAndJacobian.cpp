#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <unistd.h>
#include <cassert>
#include <fftw3.h>
#include "yorick.hpp"
#include "miniargs.hpp"
#include <sstream>

typedef float CType;
#define FFTW(x) fftwf_##x
#define MINIARG_CTYPE MINIARG_FLOAT

//#define USE_MMAP_JACOB_FIELD

CType L0;
CType h;
CType FilterRadius;

using namespace std;
using namespace HoyleVogeley;


#define REGULAR_K(r,i,N) int32_t r = (i > N/2) ? (i-(int32_t)N) : i

#define NORM_K(k) (k[0]*k[0] + k[1]*k[1] + k[2]*k[2])

#define TOPHAT_FILTER 0
#define GAUSSIAN_FILTER 1

#define FILTER_TYPE GAUSSIAN_FILTER

double j1(double x)
{
  return 3/(x*x*x) * (sin(x) - x * cos(x));
}

double myFilter(double k2)
{
#if FILTER_TYPE == TOPHAT_FILTER
    if (k2 == 0 || FilterRadius == 0)
      return 1;
    return j1(sqrt(k2)*FilterRadius/h);
#elif FILTER_TYPE == GAUSSIAN_FILTER
// Gaussian
   double r= FilterRadius/h;
  return exp(-0.5*k2*r*r);    
#else
#error Unknown filter.
#endif
}

void computeJacobianComponents(CType *allPsiField, 
			       FFTW(complex) *psiField,
			       FFTW(complex) *psiFieldHat,
			       uint32_t Nx, uint32_t Ny, uint32_t Nz,
			       CType *jacobField,
			       uint32_t l, uint32_t m,
			       CType BoxSize,
			       FFTW(plan) fwdPlan,
			       FFTW(plan) revPlan)
{
  for (uint32_t i = 0; i < Nx*Ny*Nz; i++)
    {
      psiField[i][0] = allPsiField[l+i*3];
      psiField[i][1] = 0;
    }
  
  FFTW(execute)(fwdPlan);
  
  double Normalization = 1.0/(Nx*Ny*Nz);
  double mulFactor = 2*M_PI/(BoxSize);

  for (int32_t ikz = 0; ikz < Nz; ikz++)
    {
      REGULAR_K(rz, ikz, Nz);
	  
      for (int32_t iky = 0; iky < Ny; iky++)
	{
	  REGULAR_K(ry, iky, Ny);

	  for (int32_t ikx = 0; ikx < Nx; ikx++)
	    {		  
	      REGULAR_K(rx, ikx, Nx);

	      double k_vector[] = { 
		rx, ry, rz
	      };
	      uint32_t idx = ikx + Nx*(iky+ikz*Ny);
	      
	      double tmp_r, tmp_i;
	      double k2 = NORM_K(k_vector);
	      double filt = k_vector[m] * myFilter(k2*mulFactor*mulFactor) * mulFactor;
		
	      tmp_r = -psiFieldHat[idx][1] * filt;
	      tmp_i = psiFieldHat[idx][0] * filt;

	      psiFieldHat[idx][0] = tmp_r;
	      psiFieldHat[idx][1] = tmp_i;
	      
	    }	  	      
	}
    }

  for (int32_t ikz = 0; ikz < Nz; ikz++)
    {
      int32_t rkz = (Nz-ikz) % Nz;


      for (int32_t iky = 0; iky < Ny; iky++)
	{
	  int32_t rky = (Ny-iky) % Ny;

	  for (int32_t ikx = 0; ikx < Nx; ikx++)
	    {		  
	      int32_t rkx = (Nx-ikx) % Nx;

	      uint32_t idx = ikx + Nx*(iky + ikz*Ny);
	      uint32_t ridx = rkx + Nx*(rky + rkz*Ny);
#if 0
	      if (fabs(psiFieldHat[idx][0]-psiFieldHat[ridx][0]) > 1e-11)
		abort();
	      if (fabs(psiFieldHat[idx][1]+psiFieldHat[ridx][1]) > 1e-11)
		abort();
#endif

	      if (idx < ridx)
		{
		  psiFieldHat[ridx][0] = psiFieldHat[idx][0];
		  psiFieldHat[ridx][1] = -psiFieldHat[idx][1];
		}
	    }
	}
    }

  FFTW(execute)(revPlan);

  CType imPart = 0, rePart =0;
  CType *JF = jacobField + m + 3*l;
  for (uint32_t idx = 0; idx < Nx*Ny*Nz; idx++)
    {
      imPart = max(imPart, abs(psiField[idx][1]));
      rePart = max(rePart, abs(psiField[idx][0]));
      *JF = psiField[idx][0]*Normalization;
      JF += 9;
    }
  cout << "rePart=" << rePart << endl;
  cout <<"imPart=" << imPart << endl;
}

typedef struct {
  int fd;
  size_t totalLen;
} HeaderBig;

CType *allocBigFloat(uint32_t N)
{
#ifdef USE_MMAP_JACOB_FIELD
  int fd = open("temp.jacob", O_CREAT|O_RDWR, 0600);
  long pageSize = sysconf(_SC_PAGESIZE);

  if (fd < 0)
   {
     perror("Opening jacob field");
     abort();
   }

  off_t totalLen = sizeof(CType)*(off_t)N;

  totalLen /= pageSize;
  totalLen ++;
  totalLen *= pageSize;

  HeaderBig *h;
  uint32_t off = ((sizeof(h)/pageSize) + 1) * pageSize;

  totalLen += off;

  if (ftruncate(fd, totalLen) < 0)
   {
     perror("Extending jacob field file");
     abort();
   }

  void *a = mmap(0, totalLen, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
  if (a == (void *)-1)
   {
     perror("Building the memory map");
     abort();
   }

  h = (HeaderBig *)a;
  h->fd = fd;
  h->totalLen = totalLen;
 
  size_t b = ((size_t)a) + off;

  return (CType *)b;
#else
  return new CType[N];
#endif
}

void freeBigFloat(CType *f)
{
#ifdef USE_MMAP_JACOB_FIELD
  size_t base = (size_t)f;
  long pageSize = sysconf(_SC_PAGESIZE);
  off_t off = sizeof(HeaderBig)/pageSize + 1;
  base -= off;
  HeaderBig *h = (HeaderBig *)base;
  
  munmap(h, h->totalLen);
  close(h->fd);
#else
  delete[] f;
#endif
}

int main(int argc, char **argv)
{
  const char *inFileName = argv[1];
  const char *outFileName = argv[2];
  
  MiniArgDesc desc[] = {
    { "INPUT DISPLACEMENT", &inFileName, MINIARG_STRING },
    { "OUTPUT JACOBIAN", &outFileName, MINIARG_STRING },
    { "FILTER RADIUS", &FilterRadius, MINIARG_CTYPE },
    { "BOXSIZE (Mpc/h)", &L0, MINIARG_CTYPE },
    { "h (H/(100 km/s/Mpc))", &h, MINIARG_CTYPE },
    { 0, 0, MINIARG_NULL }
  };

  if (!parseMiniArgs(argc, argv, desc))
    return 0;

  FFTW(plan) fwdPlan, revPlan;

  cout << "BoxSize is " << L0 << "*1/h100 (Mpc)  Hubble is h100=" << h << endl;
  cout << "Remember that the displacement field must be in Mpc and not in Mpc/h100 !" << endl;
  cout << "Filter radius is " << FilterRadius << " Mpc/h" << endl;
  cout << "Filter is "
#if FILTER_TYPE == TOPHAT_FILTER
       << " tophat "
#elif FILTER_TYPE == GAUSSIAN_FILTER
       << " gaussian "
#else
#error Unknown filter
#endif
       << endl;

  CType *allPsiField;
  FFTW(complex) *psiField;
  uint32_t *dimList;
  uint32_t rank;
  CType *jacobField;
  FFTW(complex) *psiFieldHat;
  CType BoxSize = L0/h;

  loadArray(inFileName,
   	    allPsiField, dimList, rank);

  assert(rank == 4);

  uint32_t Nz = dimList[0], Ny = dimList[1], Nx = dimList[2];
  cout << "(Nx,Ny,Nz) = " << Nx << " " << Ny << " " << Nz << endl;

  uint32_t multiplicator = (9+2)*Nx*Ny*Nz;
  cout << "Allocating " << multiplicator*sizeof(CType)/(1024*1024) << " Mbytes" << endl;
  jacobField = allocBigFloat(Nx*Ny*Nz*9);
  psiField = (FFTW(complex) *)FFTW(malloc)(sizeof(FFTW(complex)) * Nx * Ny * Nz);
  psiFieldHat = (FFTW(complex) *)FFTW(malloc)(sizeof(FFTW(complex)) * Nz * Ny * Nx);
  fwdPlan = FFTW(plan_dft_3d)(Nz, Ny, Nx,
			     psiField, psiFieldHat,
			     FFTW_FORWARD,
			     FFTW_ESTIMATE );
  
  revPlan = FFTW(plan_dft_3d)(Nz, Ny, Nx,
			     psiFieldHat, psiField,
			     FFTW_BACKWARD,
			     FFTW_ESTIMATE );
  
  for (uint32_t k = 0; k < 3; k++)
    {
      for (uint32_t l = 0; l < 3; l++)
	{
	  computeJacobianComponents(allPsiField, 
				    psiField,
				    psiFieldHat,
				    Nx, Ny, Nz,
				    jacobField,
				    k, l,
				    BoxSize, fwdPlan, revPlan);
	}      
    }

  {
    uint32_t outDimList[] = { Nz, Ny, Nx, 3, 3 };
    
    saveArray(outFileName,
	    jacobField, outDimList, 5);

  }  

  CType *dField = new CType[Nx*Ny*Nz];
  for (uint32_t i = 0; i < Nx*Ny*Nz; i++)
    {
      dField[i] = -(jacobField[0 + 3*0 + 9*i] + jacobField[1 + 3*1 + 9*i] + jacobField[2 + 3*2 + 9*i]);
    }

  {
   uint32_t outDimList[] = { Nz, Ny, Nx };
    
   saveArray("divergence.nc",
	   dField, outDimList, 3);
    
  }

  FFTW(destroy_plan)(fwdPlan);
  FFTW(destroy_plan)(revPlan);
  FFTW(free)(psiField);
  FFTW(free)(psiFieldHat);
  delete[] allPsiField;
  freeBigFloat(jacobField);

  return 0;
}
