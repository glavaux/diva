#ifndef __DIVA_MATCHER_HPP
#define __DIVA_MATCHER_HPP

#include <omp.h>
#include <stdint.h>
#include <vector>
#include <list>
#include <set>
#include <boost/format.hpp>
#include <boost/bind.hpp>
#include <boost/multi_array.hpp>
#include <omptl/algorithm>

namespace DIVA
{

namespace details
{

template <typename Pair> 
bool ComparePairThroughSecond(const Pair& p1, const Pair& p2) 
{  
    return p1.second < p2.second; 
} 

template<typename T>
struct ContainerCounter
{
  typedef const T& const_reference;
  typedef T& reference;
  
  uint32_t count;

  void clear() { count = 0; }
  void push_back(const T&) { count++; }
  size_t size() const { return count; }
};

};

template<typename IdType>
struct DivaMatch
{
  typedef std::list<IdType> merger_list_t;
  typedef std::vector<merger_list_t> merger_tree_t;
  
  merger_tree_t watershed_tree;
  std::vector<IdType> watershed_match;
  
  float matchThreshold, mergeThreshold;
};


template<typename Iterator, typename ProgressFun, typename IdType>
void divaMatcher(Iterator zone1_start, Iterator zone1_end, Iterator zone2_start, Iterator zone2_end, uint32_t numZone1, uint32_t numZone2, ProgressFun progress, DivaMatch<IdType>& match)
{
  using details::ComparePairThroughSecond;
  using details::ContainerCounter;
  using boost::str;
  using boost::format;
  using boost::multi_array;
  using std::set;
  using std::vector;

  typedef set<IdType> IdSet;
  typedef multi_array<IdSet, 1> IdSetArray;
  typedef std::pair<IdType, float> VoidRatio;
  
  float matchThreshold = match.matchThreshold;
  float mergeThreshold = match.mergeThreshold;
  IdType p;

  IdSetArray minSet1(boost::extents[numZone1]), minSet2(boost::extents[numZone2]);

  
  progress("Initialize matcher");

  match.watershed_tree.resize(numZone1);  
  match.watershed_match.resize(numZone1);  

  p = 0;
  while (zone1_start != zone1_end)
    {
      if (*zone1_start >= 0)
        minSet1[*zone1_start].insert(p);
      p++;
      zone1_start++;
    }
      
  p = 0;
  while (zone2_start != zone2_end)
    {
      if (*zone2_start >= 0)
        minSet2[*zone2_start].insert(p);
      p++;
      zone2_start++;
    }
    
  progress("Computing intersections");
  for (uint32_t s1 = 0; s1 < numZone1; s1++)
    {
      std::vector<VoidRatio> ratio_max(omp_get_max_threads());
      typename DivaMatch<IdType>::merger_list_t& loc_tree = match.watershed_tree[s1];
      
      if ((s1 % 10) == 0)
        progress(str(format(" Void %d / %d") % s1 % numZone1));

      fill(ratio_max.begin(), ratio_max.end(), std::make_pair(-1, 0));

#pragma omp parallel
      {
        ContainerCounter<IdType> tmp_outset;
        float& max_ratio_local = ratio_max[omp_get_thread_num()].second;
        IdType& ref_void_local = ratio_max[omp_get_thread_num()].first;
        float set1_size = minSet1[s1].size();
        
#pragma omp for schedule(static)
        for (uint32_t s2 = 0; s2 < numZone2; s2++)
          {
            tmp_outset.clear();
            std::set_intersection(
              minSet1[s1].begin(), minSet1[s1].end(), 
              minSet2[s2].begin(), minSet2[s2].end(),
              std::back_inserter(tmp_outset));

            float ratio = tmp_outset.size() / set1_size;
            
            if (ratio > max_ratio_local)
              {
                max_ratio_local = ratio;
                ref_void_local = s2;
              }
              
            if (ratio > mergeThreshold)
              {
#pragma omp critical
                loc_tree.push_back(s2);
              }
          }
      }
      
      VoidRatio ref = *omptl::max_element(ratio_max.begin(), ratio_max.end(), ComparePairThroughSecond<VoidRatio>,100);
      
//      progress(str(format("match %d / ratio = %lg") % ref.first % ref.second));
      
      if (ref.second < matchThreshold)
        match.watershed_match[s1] = -1;
      else
        match.watershed_match[s1] = ref.first;
    }
}


typedef DivaMatch<int32_t> DivaMatch_int;
typedef std::vector<std::list<int32_t> > divamatch_list;


};



#endif
